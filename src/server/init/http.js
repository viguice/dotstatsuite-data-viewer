const getUrl = server => `http://${server.address().address}:${server.address().port}`;

const init = ctx => {
  const {
    app,
    config: {
      isProduction,
      server: { host, port },
    },
  } = ctx;
  return new Promise((resolve, reject) => {
    const server = app.listen(port, host, err => {
      if (err) return reject(err);
      const url = getUrl(server);
      server.url = url;
      return resolve({ ...ctx, httpServer: server });
    });
  });
};

module.exports = init;
