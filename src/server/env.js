const dotenv = require('dotenv');
const path = require('path');
dotenv.load({ path: path.resolve(__dirname, '../..', '.env') });
