import debug from '../debug';

const getTenant = configProvider => async (req, res, next) => {
  try {
    const t0 = new Date();
    const tenantSlug = req.query.tenant || req.headers['x-tenant'];
    const [tenantId, scopeId] = tenantSlug ? tenantSlug.split(':') : [null, null];
    const tenant = await configProvider.getTenant(tenantId);
    if (!tenant) return next();
    req.member = tenant;
    debug.info(`load member '${tenant?.id}' in ${new Date() - t0} ms`);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = getTenant;
