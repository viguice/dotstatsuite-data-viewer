import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { sisccTheme } from '@sis-cc/dotstatsuite-visions';
import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core/styles';
import { Rtl } from './jss-provider';

const theme = R.pathOr({}, ['SETTINGS', 'theme'], window);

const Provider = ({ children, isRtl }) => {
  const muiTheme = R.pipe(
    R.flip(R.mergeDeepRight)(theme),
    createTheme,
  )(sisccTheme({ rtl: isRtl ? 'rtl' : 'ltr', outerPalette: theme.outerPalette }));

  return (
    <Rtl>
      <ThemeProvider theme={muiTheme}>{React.Children.only(children)}</ThemeProvider>
    </Rtl>
  );
};

Provider.propTypes = {
  isRtl: PropTypes.bool,
  children: PropTypes.element.isRequired,
};

export default Provider;
