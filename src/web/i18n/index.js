import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { FormattedMessage as RIFormattedMessage } from 'react-intl';
import Link from '@material-ui/core/Link';

const richValues = {
  br: <br />, // new line -> {br}
  i: chunks => <i>{chunks}</i>, // italic   -> <i>...</i>
  b: chunks => <b>{chunks}</b>, // bold     -> <b>...</b>
  a: (chunks = []) => {
    const [anchor, href] = R.split('|', R.head(chunks));
    return (
      <Link href={href} target="_blank" rel="noopener noreferrer">
        {anchor}
      </Link>
    );
  }, // link     -> <a>anchor|http://....</a>
};

export { default as I18nProvider } from './provider';
export const FormattedMessage = ({ values = {}, ...rest }) => (
  <RIFormattedMessage {...rest} values={{ ...values, ...richValues }} />
);

FormattedMessage.propTypes = {
  values: PropTypes.object,
};

export const formatMessage = intl => (message, values = {}) =>
  intl.formatMessage(message, { ...richValues, ...values });
