import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useIntl, defineMessages } from 'react-intl';
import { Viewer as ViewerComp, rules2 } from '@sis-cc/dotstatsuite-components';
import Link from '@material-ui/core/Link';
import { FormattedMessage, formatMessage } from '../../i18n';
import SanitizedInnerHTML from '../../components/SanitizedInnerHTML';

const messages = defineMessages({
  link: { id: 'de.viewer.copyright.content.link' },
});

const getHeaderDisclaimer = (range = {}, type, tableProps = {}) => {
  const { count, total } = range;
  const { truncated, cellsLimit } = tableProps;
  const totalCells = R.defaultTo(0, tableProps?.totalCells);
  if (count < total || truncated) {
    const values = {
      cellsLimit,
      total,
      totalCells,
      type,
    };
    if (total > cellsLimit) {
      return <FormattedMessage id="dv.incomplete.vis.data.total.limit" values={values} />;
    }
    return <FormattedMessage id="dv.incomplete.table.cells.limit" values={values} />;
  }
  return null;
};

const getBooleanValue = value => {
  if (value === 'true' || value === '1') {
    return true;
  }
  return false;
};

const formatBoolean = value =>
  value ? <FormattedMessage id="sdmx.data.true" /> : <FormattedMessage id="sdmx.data.false" />;

const cellValueAccessor = obsType => value => {
  if (R.is(Boolean, value)) return formatBoolean(value);
  else if (obsType === 'Boolean') return formatBoolean(getBooleanValue(value));
  return value;
};

const getCopyright = intl => withCopyright => {
  if (!withCopyright) {
    return null;
  }
  const label = <FormattedMessage id="de.viewer.copyright.label" />;

  const content = (
    <FormattedMessage
      id="de.viewer.copyright.content.label"
      values={{
        link: (
          <Link href={formatMessage(intl)(messages.link)} target="_blank" rel="noopener noreferrer" variant="body2">
            <FormattedMessage id="de.viewer.copyright.content.link.label" />
          </Link>
        ),
      }}
    />
  );

  return { label, content };
};

const Viewer = ({ headerProps, footerProps, range, type, tableProps, ...rest }) => {
  const intl = useIntl();
  const disclaimer = getHeaderDisclaimer(range, type, tableProps);
  const copyright = getCopyright(intl)(footerProps.withCopyright);

  const props = { range, type, tableProps, ...rest };

  return (
    <ViewerComp
      {...props}
      headerProps={R.assoc('disclaimer', disclaimer, headerProps)}
      footerProps={R.assoc('copyright', copyright, footerProps)}
    />
  );
};

const Component = ({ data, type, terms, range, mode, locale, timeFormats, display, observationsType }) => {
  const [activeCellIds, activeCellHandler] = useState(undefined);
  const labelAccessor = useCallback(
    d => {
      // old snapshots have label attribute
      const label = R.prop('label', d);
      if (label) return label;
      return rules2.getTableLabelAccessor(display)(d);
    },
    [display],
  );
  return (
    <Viewer
      {...R.over(
        R.lensProp('tableProps'),
        R.pipe(
          R.set(R.lensProp('cellValueAccessor'), cellValueAccessor(observationsType)),
          R.set(R.lensProp('activeCellIds'), activeCellIds),
          R.set(R.lensProp('activeCellHandler'), activeCellHandler),
          R.set(R.lensProp('HTMLRenderer'), SanitizedInnerHTML),
          R.set(R.lensProp('labelAccessor'), labelAccessor),
        ),
      )(data)}
      locale={locale}
      timeFormats={timeFormats}
      range={range}
      terms={terms}
      type={type}
      mode={mode}
    />
  );
};

Component.propTypes = {
  data: PropTypes.object,
  type: PropTypes.string,
  terms: PropTypes.object,
  range: PropTypes.object,
  mode: PropTypes.string,
  locale: PropTypes.string,
  timeFormats: PropTypes.object,
  observationsType: PropTypes.string,
};

export default Component;
