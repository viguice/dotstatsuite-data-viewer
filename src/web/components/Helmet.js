import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useIntl, defineMessages } from 'react-intl';
import { formatMessage } from '../i18n';

const messages = defineMessages({
  title: { id: 'dv.app.title' },
});

const View = ({ isRtl, lang }) => {
  const intl = useIntl();

  return (
    <Helmet htmlAttributes={{ lang, dir: isRtl ? 'rtl' : 'ltr' }}>
      <title>{formatMessage(intl)(messages.title)}</title>
    </Helmet>
  );
};

View.propTypes = {
  isRtl: PropTypes.bool,
  lang: PropTypes.string,
};

export default View;
