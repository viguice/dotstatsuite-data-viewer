import React from 'react';
import axios from 'axios';
import * as R from 'ramda';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import CssBaseline from '@material-ui/core/CssBaseline';
import numeral from 'numeral';
import { I18nProvider } from '../i18n';
import { getLatestData, parseHeadersRange, getFrequency, getHiddenIds } from '../lib/utils';
import { SNAPSHOT, LATEST, NO_SOURCE } from '../lib/constants';
import { shareViewEvent } from '../lib/analytics';
import Helmet from './Helmet';
import SubApp from './SubApp';
import { ThemeProvider } from '../theme';
import { getSdmxRequestArgs, getUrlLocale } from '../lib/searchUrl';

class App extends React.Component {
  state = {
    isFetching: false,
    isRtl: false,
    isLayoutIncomptaible: false,
    error: null,
    locale: 'en',
    range: null,
    type: undefined,
    terms: null,
    timeFormats: null,
    data: undefined,
    pendingConfirmation: undefined,
    notFound: undefined,
    mode: undefined,
    observationsType: undefined,
    display: undefined,
  };

  componentDidMount = () => {
    const params = new URLSearchParams(window.location.search);
    const shareId = params.get('chartId');

    if (!shareId) return this.setState({ error: new Error('no id') });

    this.setState({ isFetching: true });
    axios
      .get(`${R.path(['SETTINGS', 'share', 'endpoint'])(window)}/api/charts/${shareId}`)
      .then(res => {
        const sharedData = R.path(['data', 'data'], res);
        if (R.path(['data', 'status'])(res) === 'PENDING') {
          // return pending error message
          return this.setState({ isFetching: false, pendingConfirmation: true });
        }
        if (R.either(R.isNil, R.isEmpty)(sharedData)) {
          // return no data error message
          return this.setState({ isFetching: false });
        }
        const { isRtl, mode, type } = sharedData;
        const locale = R.pathOr({}, ['config', 'locale'], sharedData);
        this.setState({ locale });
        const terms = R.pathOr({}, ['config', 'terms'], sharedData);
        const timeFormats = R.pathOr({}, ['config', 'timeFormats'], sharedData);
        const observationsType = R.pathOr(undefined, ['config', 'observationsType'], sharedData);
        const display = R.pathOr(null, ['config', 'display'], sharedData);
        const upperType = R.toUpper(type);

        const getTitle = R.path(['headerProps', 'title', 'label']);

        if (mode === SNAPSHOT) {
          shareViewEvent({
            locale: this.state.locale,
            customLabel: `${getTitle(sharedData)} ${shareId}`,
            label: `${upperType} - ${getTitle(sharedData)} ${shareId}`,
            type: upperType,
          });
          const { range, cellsLimit } = R.propOr({}, 'config', sharedData);
          this.setState({
            isFetching: false,
            type,
            data: R.assocPath(['tableProps', 'cellsLimit'], cellsLimit)(sharedData),
            isRtl,
            terms,
            timeFormats,
            range,
            mode,
            observationsType,
            display,
          });
        }

        if (mode === LATEST) {
          const urlLocale = getUrlLocale();
          if (!R.isNil(urlLocale)) {
            this.setState({ locale: urlLocale });
          }
          const { url, headers, params } = getSdmxRequestArgs(sharedData);
          const isSDMX3 = R.pipe(R.prop('Accept'), R.includes(rules2.SDMX_3_0_JSON_DATA_FORMAT))(headers);

          const parseJson = (json, hierarchies = {}) => {
            const dataflowId = R.path(['config', 'sdmxSource', 'identifiers', 'code'], sharedData);
            const hiddenIds = getHiddenIds(json);
            const { locale, timeFormat } = R.prop('config', sharedData);
            const frequency = getFrequency(json);
            const options = { locale, timeFormat, dataflowId, frequency, hiddenIds };
            const v7json = R.pipe(rules.v8Transformer, R.prop('data'))(json, options);
            const isLayoutCompatible = rules.isSharedLayoutCompatible(v7json, sharedData);
            if (!isLayoutCompatible) {
              this.setState({ isLayoutIncomptaible: true });
              throw Error('incompatible layout');
            }
            return getLatestData(sharedData, v7json, hierarchies);
          };

          if (url && url !== NO_SOURCE) {
            axios
              .get(url, { headers, params })
              .then(response => {
                const range = SDMXJS.parseDataRange(response);
                const limit = parseHeadersRange(headers);
                this.setState({ range: { ...range, limit } });
                return R.prop('data', response);
              })
              .then(json => (isSDMX3 ? rules2.sdmx_3_0_DataFormatPatch(json) : json))
              .then(json => {
                const hCodelistsRefs = rules2.getHCodelistsRefsInData(json);
                if (!R.isEmpty(hCodelistsRefs)) {
                  return Promise.all(
                    R.map(({ codelistId, hierarchy, ...identifiers }) => {
                      const { url, headers, params } = SDMXJS.getRequestArgs({
                        identifiers,
                        datasource: R.path(['config', 'sdmxSource', 'datasource'], sharedData),
                        type: 'hierarchicalcodelist',
                      });

                      return axios.get(url, { headers, params }).then(res => {
                        const parsed = rules2.parseHierarchicalCodelist(res.data, hierarchy);
                        return { codelistId, hierarchy: parsed };
                      });
                    }, R.values(hCodelistsRefs)),
                  )
                    .then(results => {
                      const hierarchies = R.reduce(
                        (acc, { codelistId, hierarchy }) => {
                          if (R.isNil(codelistId)) {
                            return acc;
                          }
                          return R.assoc(codelistId, hierarchy, acc);
                        },
                        {},
                        results,
                      );

                      return parseJson(json, hierarchies);
                    })
                    .catch(() => parseJson(json));
                } else {
                  return parseJson(json);
                }
              })
              .then(data => {
                shareViewEvent({
                  locale: this.state.locale,
                  customLabel: `${getTitle(sharedData)} ${shareId}`,
                  label: `${upperType} - ${getTitle(data)} ${shareId}`,
                  type: upperType,
                });
                // careful, the json here is SDMX and not json from chart service
                this.setState({
                  isFetching: false,
                  type,
                  data,
                  isRtl,
                  terms,
                  timeFormats,
                  mode,
                  observationsType,
                  display,
                });
              })
              .catch(error => {
                this.setState({ isFetching: false, error });
              });
          }
        }

        const stateLocale = this.state.locale;
        const delimiters = R.pathOr({}, ['SETTINGS', 'i18n', 'locales', stateLocale, 'delimiters'], window);
        if (!R.isNil(delimiters) && !R.isEmpty(delimiters)) {
          numeral.locale(`${locale}/${locale}`);
          numeral.register('locale', `${locale}/${locale}`, { delimiters });
        }
      })
      .catch(error => {
        if (R.path(['response', 'status'])(error) === 404) {
          // return 404 not found
          this.setState({ isFetching: false, notFound: true });
        } else {
          // return error message
          this.setState({ isFetching: false, error });
        }
        throw error;
      });
  };

  render = () => (
    <ThemeProvider isRtl={this.state.isRtl}>
      <I18nProvider localeId={this.state.locale} messages={window.I18N}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Helmet lang={this.state.locale} isRtl={this.state.isRtl} />
          <CssBaseline />
          <SubApp {...this.state} />
        </div>
      </I18nProvider>
    </ThemeProvider>
  );
}

export default App;
