import '@babel/polyfill';
import React from 'react';
import { createRoot } from 'react-dom/client';
import * as R from 'ramda';
import { initialize as initializeAnalytics } from './lib/analytics';
import App from './components/App';
import meta from '../../package.json';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const analytics = R.pathOr({}, ['SETTINGS', 'analytics'])(window);
initializeAnalytics(analytics);

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<App />);
