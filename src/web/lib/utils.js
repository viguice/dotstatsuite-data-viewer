import * as R from 'ramda';
import { getDataFrequency, getNotDisplayedCombinations } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import { TABLE } from './constants';

export const getFrequency = sdmxJson => {
  const dimensions = R.pathOr([], ['data', 'structure', 'dimensions', 'observation'], sdmxJson);
  const attributes = R.pathOr([], ['data', 'structure', 'attributes', 'observation'], sdmxJson);
  return getDataFrequency({ dimensions, attributes });
};

export const getHiddenIds = sdmxJson =>
  R.pipe(R.pathOr([], ['data', 'structure', 'annotations']), R.find(R.propEq('type', 'NOT_DISPLAYED')), annot => {
    return R.isNil(annot) ? {} : getNotDisplayedCombinations(annot);
  })(sdmxJson);

export const withIndex = R.addIndex(R.map)((value, index) => R.assoc('index', index)(value));

export const getLatestData = (sharedData, sdmxJson, hierarchies) => {
  const dataflowId = R.path(['config', 'sdmxSource', 'identifiers', 'code'], sharedData);
  const dataquery = R.path(['config', 'sdmxSource', 'dataquery'], sharedData);
  const name = R.pathOr(`[${dataflowId}]`, ['structure', 'name'], sdmxJson);
  const { sdmxUrl, customAttributes, display, locale, defaultCombinations = [] } = R.propOr({}, 'config', sharedData);
  const tooltipAttributes = rules2.getDataflowTooltipAttributesIds({ data: sdmxJson }, customAttributes);
  const _customAttributes = R.merge(
    tooltipAttributes,
    R.pick(['prefscale', 'decimals', 'rejectedValueIds'], customAttributes),
  );
  const type = R.prop('type', sharedData);
  const dataflow = { id: dataflowId, name };
  const preparedData = rules2.prepareData(
    { data: sdmxJson },
    { customAttributes: _customAttributes, locale, hierarchies, dataflow, display, defaultCombinations, dataquery },
  );
  const { title, subtitle, sourceLabel } = R.pathOr({}, ['config', 'informations'], sharedData);

  const headerProps = R.pipe(
    R.when(R.always(!R.isNil(title) && !R.isEmpty(title)), R.set(R.lensProp('title'), { label: title })),
    R.when(R.always(!R.isNil(subtitle) && !R.isEmpty(subtitle)), R.set(R.lensProp('subtitle'), [{ label: subtitle }])),
  )(preparedData.header);

  //FOOTER
  const _footerProps = rules.getFooterProps({ dataflow, data: sdmxJson, sdmxUrl, display, type }, { sourceLabel });
  const footerProps = R.pipe(
    R.propOr({}, 'footerProps'),
    R.assocPath(['source', 'label'], _footerProps.sourceLabel),
  )(sharedData);

  //TABLE
  if (type === TABLE) {
    const { isTimeInverted, layoutIds, limit } = R.pathOr({}, ['config', 'table'], sharedData);
    const hasAccessibility = R.propOr(false, 'hasAccessibility', sharedData);
    const _tableProps = rules2.getTableProps({
      data: preparedData,
      layoutIds,
      display,
      customAttributes: _customAttributes,
      limit,
      isTimeInverted,
    });
    const textAlignAnnot = R.pipe(
      R.pathOr([], ['structure', 'annotations']),
      R.find(R.propEq('type', 'LAYOUT_CELL_ALIGN')),
    )(sdmxJson);
    const textAlign = R.pipe(R.either(R.prop('title'), R.path(['texts', locale])), val => (val ? R.toLower(val) : val))(
      textAlignAnnot,
    );
    return {
      headerProps,
      footerProps,
      tableProps: { ..._tableProps, cellsLimit: limit, textAlign },
      hasAccessibility,
      type,
    };
  }

  //CHART
  const chartOptions = R.propOr({}, 'chartOptions', sharedData);
  const { chartDimension, focused, map } = R.pathOr({}, ['config', 'chart'], sharedData);
  const frequency = getDataFrequency({
    dimensions: R.pathOr([], ['structure', 'dimensions', 'observation'], sdmxJson),
    attributes: R.pathOr([], ['structure', 'attributes', 'observation'], sdmxJson),
  });
  const series = rules.series(
    sdmxJson,
    type,
    focused,
    chartDimension,
    map,
    display,
    R.pipe(R.pick(['prefscale', 'decimals']), R.mapObjIndexed(R.of))(_customAttributes),
  );
  const parsedFocus = rules.parseFocus(sdmxJson, type, chartDimension, display, focused);
  return {
    chartData: { series, frequency, share: { focused: parsedFocus } },
    chartOptions,
    headerProps,
    footerProps,
    type,
  };
};

export const parseHeadersRange = headers => {
  if (R.isNil(headers.Range)) {
    return null;
  }
  const match = R.match(/values=(\d+)-(\d+)/)(headers.Range);
  if (R.isNil(match) || R.isEmpty(match) || R.length(match) < 3) {
    return null;
  }
  const [firstIndex, lastIndex] = R.tail(match);
  return lastIndex - firstIndex + 1;
};
